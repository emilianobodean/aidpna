---
title: "AID - Trabajo Práctico Nro. I"
author: "Emiliano Bodean"
date: "21 de octubre de 2018"
output: pdf_document
---

```{r setup, include=FALSE}
if(!require("readxl")) install.packages("readxl")
library(readxl)
if(!require("ggplot2")) install.packages("ggplot2")
library(ggplot2)
if(!require("gridExtra")) install.packages('gridExtra')
library(gridExtra)
if(!require("GGally")) install.packages('GGally')
library(GGally)
if(!require("dplyr")) install.packages('dplyr')
library(dplyr)
if(!require("kableExtra")) install.packages("kableExtra")
library(kableExtra)
knitr::opts_chunk$set(echo = TRUE)
```

# Ejercicio 1 (Transformaciones de datos).

Seis candidatas son evaluadas para el puesto de recepcionista en una empresa, para ello pasan por dos entrevistas. En la primera las evalúa el responsable de recursos humanos de la empresa (juez 1) y en la segunda el responsable del área de la cual van a depender (juez 2). La asignación de puntajes es en cordialidad, presencia y manejo de idiomas. Los puntajes asignados independientemente por estos jueces se encuentran en el archivo recepcionistas.xls.

```{r lectura, echo=TRUE}
recepcionistas <- read_excel("Datos/recepcionistas.xls")
kable(recepcionistas)
```

## Punto (a) 

Calcule en promedio por juez de cada una de las aspirantes. ¿Cuál le parece que seleccionaría cada uno de ellos? ¿Existe coincidencia?

```{r 1puntoA1}
juez1 = recepcionistas[c(2,3,4)]
juez2 = recepcionistas[c(5,6,7)]
Nombre = recepcionistas[c('candidatos')]
media_juez1 = round(apply(juez1,1,mean),2)
media_juez2 = round(apply(juez2,1,mean),2)
punto_a=data.frame(Nombre,media_juez1,media_juez2) # arma un entorno de datos
kable(punto_a)

```

Candidato que seleccionaría cada juez segun el promedio de notas de la tabla anterior.

```{r 1puntoA2}
candidata_juez1 = data.frame("Candidata Juez 1:",Nombre[which.max(media_juez1),1])
kable(candidata_juez1)
candidata_juez2 = data.frame("Candidata Juez 2:",Nombre[which.max(media_juez2),1])
kable(candidata_juez2)

```

Podemos observar que no hay coincidencias.

## Punto (b) 

Calcule el promedio de cada una de las aspirantes tomando en cuenta todos los rubros y ambos jueces.

```{r 1puntoB}
media_total = round(apply(recepcionistas[2:7],1,mean),2)
punto_b=data.frame(Nombre,media_total)
kable(punto_b)

```

## Punto (c) 

Transformar las puntuaciones observadas de modo tal que cada una de las seis variables tenga media 0 y dispersión 1. ¿Cuál sería el objetivo de esta transformación?

```{r 1puntoC}
punto_c = scale(recepcionistas[2:7])   # Convierte a media cero y dispersion 1
kable(punto_c)

```

Suponemos que esta transformación nos sierve pora poder comparar las notas de los diferentes jueces sin la parcilidad de cada uno.

## Punto (d) 

Transformar las puntuaciones de modo tal que cada candidata tenga para cada juez media 0 y dispersión 1. ¿Cuál sería el objetivo de esta transformación?

```{r 1puntoD}
punto_d = t(scale(t(recepcionistas[2:7])))   # Convierte a media cero y dispersion 1
kable(punto_d)

```

Suponemos que esta transformación nos sierve pora poder comparar las notas en las diferentes aptitudes.


## Punto (e) 

Grafique los perfiles multivariados de cada una de las candidatas para ambas transformaciones. ¿Qué observa?

```{r 1puntoE}
puntuac1 = data.frame(recepcionistas[,1:2],"Cordialidad",1)
puntuac2 = data.frame(recepcionistas[,1],recepcionistas[,3],"Presencia",1)
puntuac3 = data.frame(recepcionistas[,1],recepcionistas[,4],"Manejo de Idioma",1)
puntuac4 = data.frame(recepcionistas[,1],recepcionistas[,5],"Cordialidad",2)
puntuac5 = data.frame(recepcionistas[,1],recepcionistas[,6],"Presencia",2)
puntuac6 = data.frame(recepcionistas[,1],recepcionistas[,7],"Manejo de Idioma",2)
colnames(puntuac1)=c("Candidata","Nota","Aptitud","Juez")
colnames(puntuac2)=c("Candidata","Nota","Aptitud","Juez")
colnames(puntuac3)=c("Candidata","Nota","Aptitud","Juez")
colnames(puntuac4)=c("Candidata","Nota","Aptitud","Juez")
colnames(puntuac5)=c("Candidata","Nota","Aptitud","Juez")
colnames(puntuac6)=c("Candidata","Nota","Aptitud","Juez")
puntuac_juez1=rbind(puntuac1,puntuac2,puntuac3) 
puntuac_juez2=rbind(puntuac4,puntuac5,puntuac6) 

puntuac_juez1 = ggplot(puntuac_juez1,aes(x=Candidata, y=Nota, group=Aptitud, col=Aptitud))+
  geom_line(aes(x=Candidata, y=Nota, group=Aptitud, col=Aptitud)) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  ggtitle("Notas Juez 1")

puntuac_juez2 = ggplot(puntuac_juez2,aes(x=Candidata, y=Nota, group=Aptitud, col=Aptitud))+
  geom_line(aes(x=Candidata, y=Nota, group=Aptitud, col=Aptitud)) +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  ggtitle("Notas Juez 2")

gridExtra::grid.arrange(puntuac_juez1, puntuac_juez2, ncol=2)

```

# Ejercicio 2 (Tipos de variables resúmenes: (Datos: Internet. 2013)).

Se han registrado sobre 1500 individuos las variables siguientes:

* ID: número de identificación del registro.
* Nacionalidad
* Edad: cumplida en años
* Sexo: Masculino (1) Femenino (2)
* Estatura: en m
* Sitio: sitio preferido al que se conecta; 
  1 - chat 
  2 - correo electrónico 
  3 - buscadores 
  4 – software 
  5 – música 
  6 – deportes 
  7 - otros
* Uso: Tiempo de uso promedio por día en minutos
* Temperatura: media anual de la zona de residencia
* Autos: cantidad de autos en la manzana donde reside
* Cigarrillos: Cantidad de cigarrillos consumidos mientras utiliza Internet

```{r 2lectura}
internet <- read_excel("Datos/Internet2013.xls")
kable(head(internet,n=10))
```

## Punto (a) 

Clasificar las variables de la base. Para las variables numéricas construir un gráfico de coordenadas paralelas.


Clasificación:

* Nacionalidad: Categórica Nominal
* Sexo: Categórica Nominal
* Sitio: Categórica Nominal
* Edad: Cuantitaviva - Continua
* Estatura: Cuantitaviva - Continua
* Uso: Cuantitaviva - Continua
* Temperatura: Cuantitaviva - Continuoa
* Autos: Cuantitaviva - Dicreta
* Cigarrillos: Cuantitaviva - Discreta


```{r 2puntoA}

# Gráficos por variables.
plot_edad = ggplot(internet,aes(x=ID, y=Edad))+
  geom_line(aes(x=ID, y=Edad))
plot_estatura = ggplot(internet,aes(x=ID, y=Estatura))+
  geom_line(aes(x=ID, y=Estatura))
plot_uso = ggplot(internet,aes(x=ID, y=Uso))+
  geom_line(aes(x=ID, y=Uso))
plot_temp = ggplot(internet,aes(x=ID, y=Temperatura))+
  geom_line(aes(x=ID, y=Temperatura))
plot_autos = ggplot(internet,aes(x=ID, y=Autos))+
  geom_line(aes(x=ID, y=Autos))
plot_cig = ggplot(internet,aes(x=ID, y=Cigarrillos))+
  geom_line(aes(x=ID, y=Cigarrillos))
gridExtra::grid.arrange(plot_edad, plot_estatura, plot_uso, plot_temp, 
                        plot_autos, plot_cig, ncol=2, nrow=3)

# Gráfico de Coordenadas Paralelas
ggparcoord(data=internet, columns=(c(3,5,7,8,9,10)),mapping=aes(color=as.factor(
  Nacionalidad)))+
  scale_color_discrete("Nacionalidad",labels=levels(internet$Nacionalidad))+
  xlab("")+ylab("")+
  scale_x_discrete(limit=c("Edad","Estatura","Uso","Temperatura","Autos","Cigarrillos"),
                   labels=c("Edad","Estatura","Uso","Temperatura","Autos","Cigarrillos"))
```

## Punto (b) 

Construir la tabla de frecuencias de la variable sexo. ¿Hay algún valor que llame la atención? ¿Qué tipo de error considera que es?

```{r 2puntoB}
frecuencias_sexo=table(internet[["Sexo"]])
names(frecuencias_sexo)<-c("ND","Masculino","Femenino")
kable(frecuencias_sexo)
```

Tenemos un dato que no esta en el rango válido, se identifica como no definido(ND).

## Punto (c) 

- Ordenar los datos por la variable Edad. ¿Encontró algún valor extraño? ¿Qué tipo de error puede ser? 
- Construir la tabla de frecuencias de la variable Sitio. ¿Encuentra algún valor que le llame la atención? ¿Qué tipo de error puede ser?

```{r 2puntoC1}
ordenado = sort(internet[["Edad"]])
head(ordenado) #Primeros 5
tail(ordenado) #Ultimos 5

```

Vemos valores fuera de rango. Filtramos los siguientes registros estan fuera de un rango supuesto posible (menor de 6 años y mayor de 99 años). Suponemos errores de tipeo.


```{r 2puntoC2}
kable(internet[c(which(internet$Edad<6),which(internet$Edad>99)),])

```

Generamos la tabla de frecuencia de la variable sitio.

```{r 2puntoC3}
frecuencias_sitio=table(internet[["Sitio"]])
names(frecuencias_sitio)<-c(
  "chat","correo electronico","buscadores","software","musica",
  "deportes","otros")
frecuencias_sitio=data.frame(frecuencias_sitio)
kable(frecuencias_sitio)

```

Vemos que hay dos valores que no están dentro del rango definido en el enunciado (1-7). 

## Punto (d) 

Proceda de forma similar para las variables Temperatura, Autos y Cigarrillos.

Variable Temperatura:

```{r 2puntoD1}
frecuencias_temp=table(internet[["Temperatura"]])
frecuencias_temp=data.frame(frecuencias_temp)
head(frecuencias_temp, n=10)  #Primerios 10 valores
tail(frecuencias_temp, n=10)  #Últimos 10 valores

```

Variable Autos:

```{r 2puntoD2}
frecuencias_autos=table(internet[["Autos"]])
frecuencias_autos=data.frame(frecuencias_autos)
head(frecuencias_autos, n=10)  #Primerios 10 valores
tail(frecuencias_autos, n=10)  #Últimos 10 valores
```

Variable Cigarrillos:

```{r 2puntoD3}
frecuencias_cig=table(internet[["Cigarrillos"]])
frecuencias_cig=data.frame(frecuencias_cig)
head(frecuencias_cig, n=10)  #Primerios 10 valores
tail(frecuencias_cig, n=10)  #Últimos 10 valores
```


## Punto (e) 

Elimine de la base los valores que no son posibles y que seguramente corresponde a un error de tipeo. Detalle valores/registros que le hayan llamado la atención pero no deban ser eliminados necesariamente.

Registros que se consideran error de tipeo:

```{r 2puntoE1}
internet_error = internet[c(which(internet$Edad>100),
                            which(internet$Edad<0),
                            which(internet$Sexo==0),
                            which(internet$Sitio>7),
                            which(internet$Temperatura>50),
                            which(internet$Autos>1600)),]
kable(internet_error)
```

Registros que llaman la atención:

```{r 2puntoE2}
internet_raros = internet[c(which(internet$Edad==1),
                            which(internet$Temperatura<(-10)),
                            which(internet$Autos==0)),]
kable(internet_raros)
```

No se eliminan los registros porque el error esta solo un campo de estos, se deberan tener en cuenta estos errores de tipeo en el procesamiento de los datos.

## Punto (f) 

¿Para cuáles de las variables tiene sentido calcular la media? ¿Y la mediana? 

Calculo de media y mediana para las variables numéricas:
```{r 2puntoF}

internet_media = round(apply(internet[c("Edad","Estatura","Uso","Temperatura",
                                               "Autos","Cigarrillos")],2,mean),2)
internet_mediana = round(apply(internet[c("Edad","Estatura","Uso","Temperatura",
                                               "Autos","Cigarrillos")],2,median),2)
internet_media_mediana=data.frame(internet_media,internet_mediana)
kable(internet_media_mediana)
```

## Punto (g) 

¿Cuáles de las variables le parecen simétricas a partir de estos resúmenes? Confirme estas observaciones mediante un boxplot.

Las variables Estatura, Temperatura y Autos parecen ser simetricas porque la media y la mediana son casi iguales.

```{r 2puntoG}

plot2G_1=ggplot(internet,aes(y=Edad))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2G_2=ggplot(internet,aes(y=Estatura))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2G_3=ggplot(internet,aes(y=Uso))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2G_4=ggplot(internet,aes(y=Temperatura))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2G_5=ggplot(internet,aes(y=Autos))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2G_6=ggplot(internet,aes(y=Cigarrillos))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

gridExtra::grid.arrange(plot2G_1, plot2G_2, plot2G_3, plot2G_4, plot2G_5, 
                        plot2G_6, ncol=6)

```

Viendo los boxplot, también podríamos incluir a la variable Edad dentro de las simetricas.

## Punto (h) 

Calcular la desviación intercuartil y detectar presencia de valores salvajes moderados y severos.

```{r 2puntoH}

DI.edad=quantile(internet$Edad,0.75)-quantile(internet$Edad,0.25)# calcula DI
DI.estatura=quantile(internet$Estatura,0.75)-quantile(internet$Estatura,0.25)
DI.uso=quantile(internet$Uso,0.75)-quantile(internet$Uso,0.25)
DI.temp=quantile(internet$Temperatura,0.75)-quantile(internet$Temperatura,0.25)
DI.autos=quantile(internet$Autos,0.75)-quantile(internet$Autos,0.25)
DI.cig=quantile(internet$Cigarrillos,0.75)-quantile(internet$Cigarrillos,0.25)

# Valores Severos 
severos_edad = internet[c(
  which(internet$Edad>(quantile(internet$Edad,0.75)+DI.edad*3)),
  which(internet$Edad<(quantile(internet$Edad,0.25)-DI.edad*3))),] # Edad
kable(severos_edad)
severos_estatura = internet[c(
  which(internet$Estatura>(quantile(internet$Estatura,0.75)+DI.estatura*3)),
  which(internet$Estatura<(quantile(internet$Estatura,0.25)-DI.estatura*3))),] #Estatura
kable(severos_estatura)
severos_uso = internet[c(
  which(internet$Uso>(quantile(internet$Uso,0.75)+DI.uso*3)),
  which(internet$Uso<(quantile(internet$Uso,0.25)-DI.uso*3))),] # Uso
kable(severos_uso)
severos_temp = internet[c(
  which(internet$Temperatura>(quantile(internet$Temperatura,0.75)+DI.temp*3)),
  which(internet$Temperatura<(quantile(internet$Temperatura,0.25)-DI.temp*3))),] #Temp
kable(severos_temp)
severos_autos = internet[c(
  which(internet$Autos>(quantile(internet$Autos,0.75)+DI.autos*3)),
  which(internet$Autos<(quantile(internet$Autos,0.25)-DI.autos*3))),] # Autos
kable(severos_autos)
severos_cig = internet[c(
  which(internet$Cigarrillos>(quantile(internet$Cigarrillos,0.75)+DI.cig*3)),
  which(internet$Cigarrillos<(quantile(internet$Cigarrillos,0.25)-DI.cig*3))),] # Cig
kable(severos_cig)

```
```{r}
# Valores Moderados 
internet2 = internet[-c(
  which(internet$Edad>(quantile(internet$Edad,0.75)+DI.edad*3)),
  which(internet$Edad<(quantile(internet$Edad,0.25)-DI.edad*3))),] # Edad
severos_edad = internet2[c(
  which(internet2$Edad>(quantile(internet2$Edad,0.75)+DI.edad*1.5)),
  which(internet2$Edad<(quantile(internet2$Edad,0.25)-DI.edad*1.5))),]
kable(severos_edad)
internet2 = internet2[-c(
  which(internet2$Estatura>(quantile(internet2$Estatura,0.75)+DI.estatura*3)),
  which(internet2$Estatura<(quantile(internet2$Estatura,0.25)-DI.estatura*3))),] #Estatura
severos_estatura = internet2[c(
  which(internet2$Estatura>(quantile(internet2$Estatura,0.75)+DI.estatura*1.5)),
  which(internet2$Estatura<(quantile(internet2$Estatura,0.25)-DI.estatura*1.5))),]
kable(severos_estatura)
internet2 = internet2[-c(
  which(internet2$Uso>(quantile(internet2$Uso,0.75)+DI.uso*3)),
  which(internet2$Uso<(quantile(internet2$Uso,0.25)-DI.uso*3))),] # Uso
severos_uso = internet2[c(
  which(internet2$Uso>(quantile(internet2$Uso,0.75)+DI.uso*1.5)),
  which(internet2$Uso<(quantile(internet2$Uso,0.25)-DI.uso*1.5))),] 
kable(severos_uso)
internet2 = internet2[-c(
  which(internet2$Temperatura>(quantile(internet2$Temperatura,0.75)+DI.temp*3)),
  which(internet2$Temperatura<(quantile(internet2$Temperatura,0.25)-DI.temp*3))),] #Temp
severos_temp = internet2[c(
  which(internet2$Temperatura>(quantile(internet2$Temperatura,0.75)+DI.temp*1.5)),
  which(internet2$Temperatura<(quantile(internet2$Temperatura,0.25)-DI.temp*1.5))),]
kable(severos_temp)
internet2 = internet2[-c(
  which(internet2$Autos>(quantile(internet2$Autos,0.75)+DI.autos*3)),
  which(internet2$Autos<(quantile(internet2$Autos,0.25)-DI.autos*3))),] # Autos
severos_autos = internet2[c(
  which(internet2$Autos>(quantile(internet2$Autos,0.75)+DI.autos*1.5)),
  which(internet2$Autos<(quantile(internet2$Autos,0.25)-DI.autos*1.5))),]
kable(severos_autos)
internet2 = internet2[-c(
  which(internet2$Cigarrillos>(quantile(internet2$Cigarrillos,0.75)+DI.cig*3)),
  which(internet2$Cigarrillos<(quantile(internet2$Cigarrillos,0.25)-DI.cig*3))),] # Cig
severos_cig = internet2[c(
  which(internet2$Cigarrillos>(quantile(internet2$Cigarrillos,0.75)+DI.cig*1.5)),
  which(internet2$Cigarrillos<(quantile(internet2$Cigarrillos,0.25)-DI.cig*1.5))),]
kable(severos_cig)
```


# Ejercicio 5. Matriz de covarianzas: (Datos Gorriones.xls)

```{r}
gorriones <- read_excel("Datos/gorriones.xls")
kableExtra::kable_styling(kable(head(gorriones,n=10)),latex_options = c("striped", "scale_down"))
```

Para esta base de datos, interesa:

## Punto (a)

Dimensión de la base de datos (n= número de observaciones, p= cantidad de variables observadas sobre cada individuo).

```{r}
dim(gorriones) # Dimensión 
```

n = 49 Observaciones (individuos)
p = 7 variables (gorriones)

## Punto (b)

Hallar el vector de medias, la matriz de varianzas y covarianzas y la matriz de correlaciones. ¿Qué características tienen estas matrices?

Vector medias:
```{r}
media_gorriones = round(apply(gorriones[,2:7],2,mean),2)
kable(media_gorriones)
```

Matriz de varianzas y covarianzas:
```{r}
v=round(var(gorriones[,-1]),2)
kableExtra::kable_styling(kable(v),latex_options = c("striped", "scale_down"))
```
Matriz de Correlaciones:
```{r}
c=round(cor(gorriones[,-1]),2)
kableExtra::kable_styling(kable(c),latex_options = c("striped", "scale_down"))
```

Se puede observar que ambas matrices son simetricas y que en la matriz de correlación los compomentes de la diagonal principal son todos 1.

## Punto (c)

Explicar que representa el elemento m11 de la matriz de varianzas y covarianzas, ídem para el elemento m31.

* m11 - Representa la variancia del "Largo total".
* m31 - Representa la covarianza entre "Largo del pico y cabeza" y "Largo total".

## Punto (d)

Explicar que representa el elemento m22 de la matriz de correlaciones, ídem para el elemento m13.

* m22 - Representa la correlación de la variable "Extensión alar" con sigo misma, por esto vale 1.
* m31 - Representa la correlación entre "Largo del pico y cabeza" y "Largo total".

## Punto (e)

Relacionar los elementos m21;m11ym22 de la matriz de varianzas y covarianzas con el elemento m12 de la matriz de correlaciones.

## Punto (f)

Hallar una nueva variable e incorporarla en la base de Gorriones: Diferencia entre el largo total y el largo del húmero. Llamémosla: Diferencia de Largos.

```{r}

difelargos = gorriones[,2]-gorriones[,5]
gorriones2 = data.frame(gorriones,difelargos)
kableExtra::kable_styling(kable(head(gorriones2, n=10)),latex_options = c("striped", "scale_down"))
```


## Punto (g)

Calcular nuevamente el vector de medias y las matrices de varianzas y covarianzas y la matriz de correlaciones de la nueva base de datos. Relacionar el nuevo vector de medias con el anterior.

Vector medias:
```{r}
media_gorriones2 = round(apply(gorriones2[,2:7],2,mean),2)
kable(media_gorriones2)

#Comparación de vector de medias
kable(data.frame(media_gorriones,media_gorriones2))
```
Podemos ver que los vectores de medias son iguales.

Matriz de varianzas y covarianzas:
```{r}
v2=round(var(gorriones2[,-1]),2)
kableExtra::kable_styling(kable(v2),latex_options = c("striped", "scale_down"))
```
Matriz de Correlaciones:
```{r}
c2=round(cor(gorriones2[,-1]),2)
kableExtra::kable_styling(kable(c2),latex_options = c("striped", "scale_down"))
```


## Punto (h)

Hallar la traza de las cuatro matrices. Explicar el significado de cada uno de los resultados. ¿Qué traza/s no aumentan al aumentar una variable? Explique.

Trazas de Matrices de varianzas y covarianzas:
```{r}
sum(diag(v))
sum(diag(v2))
```
Trazas de Matrices de Correlaciones:
```{r}
sum(diag(c))
sum(diag(c2))
```

La traza de la segunda matriz es mayor porque tenemos una variable mas. Para el caso de la matriz de correlación la traza es igual a la cantidad de variables porque en la diagonal principal todos los componentes son unos.
