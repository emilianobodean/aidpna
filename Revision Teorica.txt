

Revisión Teorica:
----------------
Indicar el valor de verdad de las siguientes premisas

a) Falso. Tambien hay multivariados, ver metodo de mahalanobis y otros.

b) Verdadero. Pag 76/77.

c) Verdadero. Pag 143. Pag 135 para p-valor.

d) Falsa. Se debe trabajar con la matriz de correlaciones. Pag 86. 

e) Falso. Los criterios se basan en la proporción de variabilidad. Pag 90.

f) Verdadero. Pag 92.

g) Falso. Tiene p componentes (p es la cantiadad de variables).

h) Falso. Es una tecnica descriptiva que no requiere ningun supuesto sobre las variables. Pag 157.

i) Falso. La suma del cuadrado de los residuos es igual al chi cuadrado. La suma de los autovalores en la inercia. Pag 183 / 188

j) Falso. 

AFC - Analisis Factorial de Correspondencia.


1 Responder las siguientes preguntas

a) Cuando la varible que se incorpora tiene influencia o no (puede estar mal). Pag 189.

b) Ver tabla Pag 147.

c) Determinar tipo de independencia. Pag 188.

d) La variabilidad de...  Pag 190.

e) Ver errores inundados y enmascarados. Pag 28. Distancia de Mahalanobis y MCD. Pag 56.

f) Hacer comparables las mediciones de distintas variables... Pag 47/48.

g) Son cuadradas y simetricas. La de correlacion esta estandarizada entre -1 y 1. La traza de la de correlacion me da la cantidad de variables. Pag 47 / 54 / ver.

h) Distancia entre individuos.
Busqueda de grupos o patrones.
Proyecciones contra los ejes.
Viendo componentes de tamaño y de forma.
Pag 

i) Pag 175 / 176.


Para ver (segun Roxana):
- Definición de inercia. Relacion con chi cuadrado
- Cap 4. Test de hipotesis, error tipo 1 -> Ver potencia del estudio.
- Interpretación de biplot.
- Que es una matriz diyuntiva.
- Todos los graficos.
- 






Ver:
----

https://rpubs.com/

Guias de R de Universidad de Granada.