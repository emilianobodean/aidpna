
##TP3

#A
usaTw=c(14,25)   #Usa Twitter
nousaTw=c(159,152)   #NO usa Twitter
tabla=rbind(usaTw,nousaTw)
colnames(tabla)=c("F","M")
tabla

mosaicplot(tabla,color=c("violet","deeppink"))

#B obtenga los porcentajes por fila y compare los diferentes usos

tot.filas=apply(tabla, 1,sum)
tot.filas
tabla/tot.filas

#C por columna

tot.col=apply(tabla, 2,sum)
tot.col
tabla/rbind(tot.col,tot.col)


#D

pruebatw = chisq.test(tabla)
names(pruebatw)
pruebatw$observed
pruebatw$expected
pruebatw$residuals
pruebatw$stdres


#E

pruebatw$p.value




#Ej 3:


esp = matrix(c(20,6,4,10,18,22,6,19,12,15,14,23,17,13,11,40,67,56,35,92), nrow = 4, ncol = 5)

esp


# A - Test de homogeneidad

Xsq=chisq.test(esp)
Xsq
Xsq$expected

# B - 






# Ej 5

tab=rbind(GerSenior=c(4,2,3,2),
          GerJunior=c(4,3,7,4),
          EmpSenior=c(25,10,12,4),
          EmpJunior=c(18,24,33,13),
          Secret=c(10,6,7,2))
colnames(tab)=c("no","poco","madio","mucho")
tab
GerSenior=c(4,2,3,2)
GerJunior=c(4,3,7,4)
EmpSenior=c(25,10,12,4)
EmpJunior=c(18,24,33,13)
Secret=c(10,6,7,2)

datos=data.frame("Frec"=c(GerSenior, GerJunior, EmpSenior,
                          EmpJunior, Secret),
                "Puesto"=c(rep("GerSenior",4),rep("GerJunior",4),
                           rep("EmpSenior",4),rep("EmpJunior",4),
                           rep("Secret",4)),
                "Fum"=factor(rep(c("no","poco","medio","mucho"),5)))
library(ggplot2)
plot1=ggplot(datos,aes(x=Fum,y=Frec,group=Puesto,color=Puesto))+
  geom_line(linetype="dashed")

plot1


#2
install.packages("ca")
install.packages("FactoMineR")
install.packages("factoextra")
library(ca)
library(FactoMineR)
library(factoextra)

tab.ac=CA(tab,graph=FALSE)
get_ca_row(tab.ac) # Muestra l o que s e guarda de l a s f i l a s
get_ca_col(tab.ac) # Muestra l o que s e guarda de l a s columnas

fviz_contrib(tab.ac, choice="row", axes=1,
             fill="royalblue", color= "black") +
  theme_gray( ) +
  theme(axis.text.x = element_text(angle=0)) +
  xlab('Nivel de atención' ) +
  ylab('Contribuciones (%)') +
  ggtitle(' ')

# Gr af i ca l a s c a t e g o r í as de l a s f i l a s
fviz_contrib(tab.ac ,choice="col",axes=1,
             fill="royalblue",color="black") +
  theme_gray( ) +
  theme(axis.text.x = element_text(angle=0)) +
  xlab('Nivel cultu r a l ' ) +
  ylab( ' Cont r ibuc ione s (%) ' ) +
  ggtitle( ' ' )

# Gr af i ca l a s c a t e g o r í as de l a s columnas
fviz_ca_row(tab.ac , repel=TRUE, col.row="royalblue" ) +
  theme_gray( ) +
  xlab( 'Dimensi ón 1 (95.4%) ' ) +
  ylab( 'Dimensi ón 2 (2.9%) ' ) +
  ggtitle ( ' ' )

# Gr af i ca l o s puntos f i l a
fviz_ca_col(tab.ac ,repel=TRUE,col.col="indianred" ) +
  theme_gray( ) +
  xlab('Dimensi ón 1 (95.4%) ' ) +
  ylab( 'Dimensi ón 2 (2.9%) ' ) +
  ggtitle( ' ' )

# Gr af i ca l o s puntos columna
fviz_ca_biplot(tab.ac ,repel=TRUE, col.row="royalblue" ,
                         col.col="indianred" ) +
  theme_gray( ) +
  xlab( 'Dimensi ón 1 (95.4%) ' ) +
  ylab( 'Dimensi ón 2 (2.9%) ' ) +
  ggtitle( ' ' )

# Re a l i z a e l b i p l o t simé t r i c o
# Apl icamos ahora e l paquete ca
tab_ac=ca(tab,graph=FALSE) # Re a l i z a e l aná l i s i s de c o r r e spondenc i a s
summary(tab_ac)
tab_ac$rowcoord # Ar roja l a s coordenadas de l b i p l o t de l a s f i l a s
tab_ac$colcoord # Ar roja l a s coordenadas de l b i p l o t de l a s columnas

# Ej 7

install.packages("ade4")
install.packages("anacor")
library(readxl)
library(ade4)
library(anacor)
ingleses <- read_excel("Datos/ingleses.xlsx")
View(ingleses)

