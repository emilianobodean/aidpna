# Ejercicio 3. Si se desea obtener cinco agrupamientos de los datos correspondientes
# a la tabla ’pizzas.xls’:

library(readxl)
Pizzas <- read_excel("Datos/Pizzas.xls", 
                     col_types = c("numeric", "numeric", "numeric", 
                                   "numeric", "numeric", "numeric", 
                                   "numeric", "numeric", "numeric"))
View(Pizzas)

#   (a) Realice un Análisis en Componentes Principales. ¿Qué proporción de
# la variabilidad total en las variables medidas explican las dos primeras
# componentes? Utilizando un gráfico de individuos determinar grupos
# en los datos. ¿Cuántos grupos hay? ¿Cuáles pizzas pertenecen a cuáles
# agrupamientos? Comparar con el ítem anterior.
library(dplyr)

# Como hay muchos datos NA, se puede eliminar las filas sin datos y calcular PCA.
# Luego si dos variables estan muy correlacionadas, se puede eliminar una vairable
# en lugar de eliminar tantos registros.

Pizzas = Pizzas %>% filter_all(all_vars(!is.na(.)))

#Calcula PCA
pizzas.pca=prcomp(Pizzas[,3:9],center=T,scale=T)
pizzas.pca

summary(pizzas.pca)

# las dos primeras componentes explican el 94%

#GRacico solo para ver
pairs(Pizzas[,3:9])

# Grafico de biplot
library(ggbiplot)
ggbiplot(pizzas.pca,choices=1:2,scale=1,pc.biplot=TRUE)


#################### Prueba
datos = Pizzas[,3:9]
# Matriz de distancias euclideas 
mat_dist <- dist(x = datos, method = "euclidean") 

# Dendrogramas  
hc_complete <- hclust(d = mat_dist, method = "complete") 
hc_average <- hclust(d = mat_dist, method = "average")
hc_single <- hclust(d = mat_dist, method = "single")
hc_ward <- hclust(d = mat_dist, method = "ward.D2")
cor(x = mat_dist, cophenetic(hc_complete))
cor(x = mat_dist, cophenetic(hc_average))
cor(x = mat_dist, cophenetic(hc_single))
cor(x = mat_dist, cophenetic(hc_ward))

plot(hc_average)
rect.hclust(hc_average, k=4, border="red")
grupos<-cutree(hc_average,k=3)
split(rownames(datos),grupos)

#####################

# (b) Aplique un método de agrupamiento a los resultados del ítem anterior
# (valores de los casos sobre las componentes)

#  clustering jer?rquico definiendo  4 grupos

library(FactoMineR)
library(factoextra) 

res <- hcut(Pizzas[,3:9], k = 4, stand = TRUE)
res$cluster
# Visualizando el dendrograma
fviz_dend(res, rect = TRUE)

fviz_cluster(res)


# (c) Aplique el método de K-Medias a los datos de manera de obtener 5 grupos.
# Compare con los resultados anteriores.
datos = Pizzas[,3:9]
res.hk <-hkmeans(datos, 5)

#visualizando el ?rbol
hkmeans_tree(res.hk, cex = 0.6)
fviz_dend(res.hk, cex = 0.6)
# Visualizando la clusterizaci?n final de hkmeans
fviz_cluster(res.hk,ellipse = TRUE ,outlier.color = "black")


# (d) Resuma los resultados: ¿tienen los datos una estructura como para agruparlos?
#   En el caso de que su respuesta sea afirmativa: ¿en cuántos grupos
# le parece más conveniente? Justifique. .

# Si, los datos tienen una estructura para ser agrupadados.
# 
