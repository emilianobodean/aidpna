---
title: "TP02_2018"
author: "Emiliano"
date: "26 de octubre de 2018"
output: pdf_document
---

```{r setup, include=FALSE}
if(!require("readxl")) install.packages("readxl")
library(readxl)
if(!require("ggplot2")) install.packages("ggplot2")
library(ggplot2)
if(!require("gridExtra")) install.packages('gridExtra')
library(gridExtra)
if(!require("GGally")) install.packages('GGally')
library(GGally)
if(!require("dplyr")) install.packages('dplyr')
library(dplyr)
if(!require("kableExtra")) install.packages("kableExtra")
library(kableExtra)
knitr::opts_chunk$set(echo = TRUE)
```

# Ejercicio 1. Sea la matriz de varianzas y covarianzas poblacionales:

```{r}
mat = matrix(c(3,1,1,1,3,1,1,1,5), nrow = 3, ncol = 3)
mat

```

Correspondiente al vector aleatorio X = (X1;X2;X3)0 de media 0.

## (a)

Hallar los autovalores y autovectores de la matriz de varianzas y covarianzas.

```{r}
mat.cov = cov (mat)
autovalores = eigen(mat.cov)$values
autovectores = eigen(mat.cov)$vector
eigen(mat.cov)
```

## (b)

Escribir la expresión de las componentes principales Y = (Y1; Y2; Y3)' e indique que proporción de la variabilidad explica cada una de ellas.

```{r}

porc=round(rbind("cada.uno"=(autovalores)/sum(autovalores),
                 "acumulado"=cumsum(autovalores)/sum(autovalores)),3)

colnames(porc)=c("lambda1","lambda2","lambda3")
porc

```

## (c)

Hallar los loadings de la primer componente principal.

```{r}
loadings = autovectores[,1]
loadings
```

## (d)

Hallar los scores de las primeras dos componentes principales correspondientes a la observación X=(2,2,1).

```{r}
# combinación lineal del vector del enunciado con el vector de loadings.
loadings = autovectores[,1]
score1 = loadings[1] * 2 + loadings[2] * 2 + loadings[3] * 1 
score1
loadings = autovectores[,2]
score2 = loadings[1] * 2 + loadings[2] * 2 + loadings[3] * 1 
score2
```


Ejercicio 2. Considerando los datos de la base chalets.xls, se pide:

```{r}
library(readxl)
chalets <- read_excel("Datos/chalets.xls", 
    col_names = FALSE, skip = 1)
View(chalets)

```

a) Graficar el boxplot de cada una de las variables. Indicar, si se observa, la
presencia de valores atípicos.
```{r}

plot2_1=ggplot(chalets,aes(y=X__1))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2_2=ggplot(chalets,aes(y=X__2))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2_3=ggplot(chalets,aes(y=X__3))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))
plot2_4=ggplot(chalets,aes(y=X__4))+geom_boxplot()+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

gridExtra::grid.arrange(plot2_1, plot2_2, plot2_3, plot2_3, ncol=4)

```



b) Graficar los diagramas de dispersión de las variables de a pares. Estimar la
presencia de correlación entre variables a partir de estos gráficos, indicando
si le parece fuerte y el signo de las mismas.
```{r}

```

c) Calcular el vector de medias y la matriz de varianzas y covarianzas muestral.

Vector medias:
```{r}
media_chalets = round(apply(chalets[,2:4],2,mean),2)
kable(media_chalets)
```
Matriz de varianzas y covarianzas:
```{r}
v=round(var(chalets[,-1]),2)
kableExtra::kable_styling(kable(v),latex_options = c("striped", "scale_down"))

```




d) Hallar la matriz de correlación muestral. Verificar las estimaciones realizadas
visualmente.

Matriz de Correlaciones:
```{r}
c=round(cor(chalets[,2:4]),2)
kableExtra::kable_styling(kable(c),latex_options = c("striped", "scale_down"))
```

e) A partir de estas observaciones, le parece razonable pensar en un análisis
de componentes principales para reducir la dimensión del problema?.

```{r}

```


f) Hallar la primera componente principal y graficar sus coeficientes mediante
barras verticales.

```{r}

chale.pca=prcomp(chalets[,2:4],centrer=T,scale=T)
load=chale.pca$rotation[,1]
load=data.frame("load"=load, "x"=c(1,2,3))
ggplot(load,aes(x,y=load)) + 
  geom_bar(stat="identity", color="red", alpha=0.3)


```



g) Indicar qué porcentaje de la variabilidad total logra explicar esta componente.
Explicar si se trata de una componente de tamaño o de forma. Es
posible ordenar las promotoras en función de esta componente?. Si la respuesta
es afirmativa, cual es la mayor y cual la menor; si es negativa, explicar
por qué no es posible ordenarlos

```{r}
chale.pca$sdev^2/sum(chale.pca$sdev^2)
```




Ejercicio 3. Dado el siguiente conjunto de datos:

```{r}
mat = matrix(c(3,5,10,6,6,12), nrow = 3, ncol = 2)
mat

```

a) Calcule la matriz de covarianza, los autovalores y autovectores.

Matriz de varianzas y covarianzas:
```{r}
v=round(var(mat),2)
#kableExtra::kable_styling(kable(v),latex_options = c("striped", "scale_down"))
v
```

```{r}
val=eigen(v)$values
vec=eigen(v)$vector
val
vec
```


b) Las componentes principales y su contribución porcentual a la varianza
total.

```{r}
sum(diag(v))  #traza de la matriz de covarianzas
diag(v)/sum(diag(v))  # proporcion de la varianza total de las var original
mat.pca.cov=prcomp(mat,center=T)
mat.pca.cov
```



c) Grafique los datos en R22 en la base original yen la base de los dos primeros
ejes.

```{r}
datos=data.frame(mat)
ggplot(datos,aes(x=X1,y=X2))+geom_point()

```

d) Repita los cálculos con los datos estandarizados. Interprete los resultados
obtenidos

```{r}

cor(mat)
mat.pca.cor=prcomp(x,center=T,scale. = T)
mat.pca.cor

```


e) Verifique que los dos primeros autovectores son ortogonales entre sí. 

```{r}

```

Represente
gráficamente estos dos vectores en un gráfico bidimensional y trace
rectas desde el origen hasta la ubicación de cada uno de los vectores en el
gráfico.


```{r}

datos1= data.frame(mat)
origin=ggplot(datos1,aes(x=X1, y=X2))+geom_point()



```


## Ejercicio 4. Sea S la matriz de varianzas y covarianzas poblacionales:


```{r}
s = matrix(c(3,1,1,1,4,0,1,0,2), nrow = 3, ncol = 3)
s

```

Correspondiente al vector aleatorio X = (X1;X2;X3)0 donde:
X1 puntuación media obtenida en las asignaturas de econometría
X2 puntuación media obtenida en las asignaturas de derecho
X3 puntuación media obtenida en asignaturas libres
Los datos corresponden a un conjunto d alumnos de la carrera de economía.


a) Calcule los autovalores de la matriz S.

```{r}

val=eigen(s)$values
vec=eigen(s)$vector
val
vec

```


b) Interprete la segunda componente principal sabiendo que el autovector correspondiente:
e1 = (0,5744 ; -0,5744; 0,5744).

Es una componente de forma.


c) Como se debería interpretar si un estudiante tuviera en la segunda componente principal una puntuación muy inferior a la de sus compañeros?.

R: Tuvo un buen desempeño en derecho y un mal desempeño en las otras asignaturas.


d) ¿Cuántas componentes principales serán necesarias para explicar al menos
el 80% de la variabilidad total del conjunto?

```{r}

s.pca=prcomp(s,centrer=T,scale.=T)
load=s.pca$rotation[,1]
load=data.frame("load"=load, "x"=c(1,2,3))
ggplot(load,aes(x,y=load)) + 
  geom_bar(stat="identity", color="red", alpha=0.3)

```



```{r}


s.pca$sdev^2/sum(s.pca$sdev^2)


```
```{r}
library(ggbiplot)
print(ggscreeplot(s.pca))
```

